package ch.clip.auth.services.controller;

import ch.clip.auth.services.model.JwtRequest;
import ch.clip.auth.services.model.JwtResponse;
import ch.clip.auth.services.service.UserService;
import ch.clip.auth.services.utilities.JWTUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class HomeController {

    private final JWTUtility jwtUtility;
    private final AuthenticationManager authenticationManager;
    private final UserService userService;

    @Autowired
    public HomeController(JWTUtility jwtUtility,
                          AuthenticationManager authenticationManager,
                          UserService userService){
        this.jwtUtility = jwtUtility;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @GetMapping("/")
    public ResponseEntity<String> home() {
        return ResponseEntity.ok("Hello World");
    }

    @PostMapping("/authenticate")
    public JwtResponse authenticate(@RequestBody JwtRequest jwtRequest) throws Exception {
        // TODO: delete this output
        System.out.println("authenticate");
        System.out.println("jwtRequest.username: " + jwtRequest.getUsername());
        System.out.println("jwtRequest.password: " + jwtRequest.getPassword());
        try {
            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(
                            jwtRequest.getUsername(),
                            jwtRequest.getPassword()));
            final UserDetails userDetails = userService.loadUserByUsername(jwtRequest.getUsername());
            final String token = jwtUtility.generateToken(userDetails);
            return new JwtResponse(token);
        } catch (BadCredentialsException badCredentialsException) {
            throw new Exception("bad credential", badCredentialsException);
        }
    }
}
