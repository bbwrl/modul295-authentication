package ch.clip.auth.services.docfolder;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("docfolders")
public class RestDocFolderController {

    // TODO: deklare some dummy Data

    // TODO: insert some dummy Data in the default constructor


    @GetMapping("")
    public ResponseEntity<List<DocFolder>> getDocFolders() {
        // TODO: use some dummy Data
        return ResponseEntity.ok(new ArrayList<>());
    }

    @GetMapping("{id}")
    public ResponseEntity<DocFolder> getDocFolder(@PathVariable int id) {
        // TODO: get the docFolder from the dummy list
        return ResponseEntity.ok(new DocFolder());
    }
}
