package ch.clip.auth.services.docfolder;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.swing.text.Document;
import java.util.List;

@Data
@NoArgsConstructor
public class DocFolder {
    private int id;
    private String title;
    private int sortOrder;
    private List<Document> documents;
}
