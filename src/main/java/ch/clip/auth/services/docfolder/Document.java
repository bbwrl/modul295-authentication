package ch.clip.auth.services.docfolder;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Document {
    private int id;
    private String title;
    private int sortOrder;
    private String documentUrl;
}
